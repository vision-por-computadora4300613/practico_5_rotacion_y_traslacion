# Practico_5_Rotación_y_Traslacion

---------------------------------------------------------------------------------------------------
## Rotación y Traslación (Transformación Euclideana)


1. Agregar a la función anterior un parámetro que permita aplicar un escalado a la porción rectangular de imagen.
Parámetros:
- angle: Ángulo
- tx: traslación en x
- ty: traslación en y

Recordar que la transformación de similaridad tiene la siguiente forma:

[s· cos(angle) s·sin(angle) tx]
[-s·sin(angle) s· cos(angle) ty]

2. Usando como base el programa anterior, escribir un programa que permita seleccionar
una porción rectangular de una imagen y
con la letra “e” aplique una transformación euclidiana a la porción de imagen
seleccionada (solicitando al usuario los tres parámetros, ángulo, traslación en x y
traslación en y) y la guarde como una nueva imagen.
---------------------------------------------------------------------------------------------------

## How to use it

En la terminal escribir:

        python3 transEuclidea.py

Aparecerá una imagen de Messi. El menú con todas las opciones aparece en la terminal.