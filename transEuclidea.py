# %% [markdown]
# ---------------------------------------------------------------------------------------------------
# ## Rotación y Traslación (Transformación Euclideana)
# 
# 
# 1. Agregar a la función anterior un parámetro que permita aplicar un escalado a la porción rectangular de imagen.
# Parámetros:
# - angle: Ángulo
# - tx: traslación en x
# - ty: traslación en y
# 
# 
# Recordar que la transformación de similaridad tiene la siguiente forma:
# 
# [s· cos(angle) s·sin(angle) tx]
# [-s·sin(angle) s· cos(angle) ty]
# 
# 1. Luego, usando como base el programa anterior, escribir un programa que permita seleccionar una porción rectangular de una imagen y con la letra “s” aplique una transformación de similaridad a la porción de imagen seleccionada (solicitando al usuario los cuatro parámetros, escala, ángulo, traslación en x y traslación en y) y la guarde como una nueva imagen.
# ---------------------------------------------------------------------------------------------------

# %% [markdown]
# Importamos La librería y declaramos las variables:
# 
#     - original: nunca cambia.
#     - clone: es la que está cambiando todo el tiempo.
#     - cropped: es la va acortándose. 
#     - buffer: es la que se va a guardar cuando soltemos el click.
#     - imgRotada: tiene la transformación de rotación.
#     - imgTrasladada: tiene la img de rotación Y traslación.

# %%
#! /usr/bien/env python
# -*- coding: utf-8-*-

import cv2 
import numpy as np

drawing = False     #true if mouse is pressed
ix, iy = -1, -1

original = cv2.imread("messi.jpg", cv2.IMREAD_COLOR)
clone = original.copy()
cropped = original.copy()
buffer =  original.copy()
imgRotada = original.copy()
imgTrasladada = original.copy()

############################################################## variables para rotación y traslación 
angle = 0
tx = 0
ty = 0
center = None

##############################################################

# %% [markdown]
# *draw_rectangle* para dibujar un rectángulo.
# 
# Esta es la función callback. Esta si o si debe tener la siguiente firma: onMouse(event, x, y, flags, param).
# 
# Aunque en este caso específico no se utilicen los parámetros flags y param, es importante mantenerlos en la firma de la función draw_circle porque la función cv2.setMouseCallback espera que la función callback tenga esa firma específica.
# 
# img, clone y original tienen que estar como globales para que se entienda que son las que declaramos antes.

# %%

def draw_rectangle(event, x, y, flags, param):
    global ix, iy, drawing, cropped, clone, buffer
    

    if event == cv2.EVENT_LBUTTONDOWN:
        drawing = True      
        ix, iy = x, y
        clone = cropped.copy()      #Para actualizar la imagen y que no se superpongan los rectángulos

        
    elif event == cv2.EVENT_LBUTTONUP:
        drawing = False
        clone = cropped.copy()       #Para actualizar la imagen
        cv2.rectangle(clone, (ix, iy), (x, y), (0, 255, 0), 3)      ##cv2.rectangle(img, pto1, pto2, color, grosor, tipo de linea)

        if ix<x and iy<y:        # Para poder hacer el rectángulo de izquierda a derecha o viceversa y de abajo para arriba o viceversa.
            buffer = cropped[iy:y, ix:x]
        elif ix<x and iy>y:
            buffer = cropped[y:iy, ix:x]
        elif ix>x and iy<y:
            buffer = cropped[iy:y, x:ix]
        else:                               #ix>x and iy>y
            buffer = cropped[y:iy, x:ix]
        
        
        
    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing is True:
            clone = cropped.copy()   #Para actualizar la imagen
            cv2.rectangle(clone, (ix, iy), (x, y), (0, 255, 0), 3)


# %% [markdown]
# 
# *cv2.namedWindow:* crea una ventana con un nombre y tamaño modificable para mostrar videos o imagenes que les pasemos por parámetro.
# 
# *cv2.setMouseCallback:* 1er parámetro, donde se va a fijar si hubo un evento. 2do parámetro, que va a hacer.

# %% [markdown]
# ## ROTACIÓN

# %%
def rotate(image, angle, center = None, scale = 1):
    (h,w) = image.shape[:2]

    if center is None:
        center = (w/2, h/2)
    
    M = cv2.getRotationMatrix2D(center, angle, 1)

    rotated = cv2.warpAffine(image, M, (w, h))
    
    return rotated

# %% [markdown]
# ## Traslación

# %%
def translate(image, x, y):
    (h,w) = (image.shape[0], image.shape[1])

    M = np.float32([[1, 0, x],
                    [0, 1, y]])
    shifted = cv2.warpAffine(image, M, (w, h))
    return shifted

# %%

cv2.namedWindow('image')
cv2.setMouseCallback('image', draw_rectangle) 


# %% [markdown]
# LLamamos constantemente a la función imshow para mostrar la imagen. 
# 
#     . Con 'r' restauramos.
#     . Con 'g' guardamos.
#     . Con 'q' salimos.
#     . Con 'e' hacemos transformaciones de rotación y traslación.
# Luego destruimos todas las ventanas con destroyAllWindows().

# %%
print("-------------------------------------------------------\n")
print("MENU: \n")
print("- q: Salir\n")
print("- g: Seleccionar rectángulo\n")
print("- r: Restaurar imágen original\n")
print("- e: Rotar y trasladar\n")
print("-------------------------------------------------------\n")

while(1):
    cv2.imshow('image', clone)
    k = cv2.waitKey(1) & 0xFF

    if k == ord('r'):
        cropped = original.copy()
        clone = original.copy()
        
    elif k == ord('g'):
        cropped = buffer.copy()
        clone = cropped.copy()
        cv2.imwrite("cropped.jpg", buffer)
        
    elif k == ord('q'):
        break

    elif k == ord('e'):
        
        angle = float(input("Angulo a rotar: "))
        tx = int(input("Traslacion(x): "))
        ty = int(input("Traslacion(y): "))
        
        imgRotada = rotate(buffer, angle, center, 1)
        imgTrasladada = translate(imgRotada, tx, ty)

        cropped = imgTrasladada.copy()
        clone = cropped.copy()
        cv2.imwrite("transEuclidea.jpg", imgTrasladada)


    
cv2.destroyAllWindows()
 


